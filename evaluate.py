import configargparse

import torch
from torch.utils.tensorboard import SummaryWriter
import numpy as np
from tqdm import trange, tqdm
from flatland.envs.observations import TreeObsForRailEnv, LocalObsForRailEnv
from flatland.envs.predictions import ShortestPathPredictorForRailEnv
from flatland.envs.rail_env import RailEnv
from flatland.envs.rail_generators import complex_rail_generator, sparse_rail_generator
from flatland.envs.schedule_generators import complex_schedule_generator, sparse_schedule_generator
try:
    from flatland.utils.rendertools import RenderTool
except:
    pass

import agents
import utils

from observations import GlobalObsForRailEnv, VectorizedTreeObsForRailEnv

if __name__ == "__main__":
    parser = configargparse.ArgumentParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    parser.add_argument(
        "--config", type=str, required=False, is_config_file=True,
        help="Configuration specifying the arguments to run the environment with."
    )

    parser.add_argument(
        "--num-trials", type=int, required=False, default=250,
        help="The amount of episodes to train the agent for."
    )
    parser.add_argument(
        "--max-steps", type=int, required=False, default=500,
        help="The maximum amount of steps that an episode will last for."
    )
    parser.add_argument(
        "--environment-size", type=int, nargs=2, required=False, default=[10, 10],
        help="The size of the environment in which to run the agents."
    )
    parser.add_argument(
        "--agent-count", type=int, required=False, default=1,
        help="The number of agents to run in the environment."
    )
    parser.add_argument(
        "--seed", type=int, required=False, default=1,
        help="The seed to use for the various random number generators."
    )
    parser.add_argument(
        "--render", type=str, choices=["off", "on"], required=False, default="on",
        help="Whether or not to render the agents while training."
    )
    parser.add_argument(
        "--screen-size", type=int, nargs=2, required=False, default=[600, 600],
        help="The size of the screen to render to."
    )
    parser.add_argument(
        "--agent-style", type=str, choices=["single", "multi"], default="single", required=False,
        help="The type of agent to train in the environment."
    )
    parser.add_argument(
        "--model-checkpoint", type=str, required=True,
        help="The model checkpoint to load."
    )

    args = parser.parse_args()

    device = torch.device(args.device) if torch.cuda.is_available() else torch.device("cpu")

    tree_observer = VectorizedTreeObsForRailEnv(
        max_depth=1,
        predictor=ShortestPathPredictorForRailEnv()
    )

    environment = RailEnv(
        width=args.environment_size[0],
        height=args.environment_size[1],
        rail_generator=complex_rail_generator(
            nr_start_goal=10,
            nr_extra=2,
            min_dist=8,
            max_dist=99999,
            seed=args.seed
        ),
        schedule_generator=complex_schedule_generator(),
        number_of_agents=args.agent_count,
        obs_builder_object=tree_observer
    )

    environment.reset()

    if args.render == "on":
        environment_renderer = RenderTool(
            environment,
            gl="PILSVG",
            screen_width=args.screen_size[0],
            screen_height=args.screen_size[1]
        )

    agent = agents.MlpAgent(
        (args.agent_count if args.agent_style == "multi" else 1) * 60,
        (args.agent_count if args.agent_style == "multi" else 1) * 5,
        1,
        0,
        0.0,
        1,
        device,
        None
    )

    agent.load(args.model_checkpoint)

    for trial in trange(1, args.num_trials + 1):
        observations, info = environment.reset(
            regenerate_rail=False,
            regenerate_schedule=False
        )

        for idx in range(args.agent_count):
            tmp_agent = environment.agents[idx]
            tmp_agent.speed_data["speed"] = 1
            tmp_agent.speed_data["position_fraction"] = 0.0

        if args.render == "on":
            environment_renderer.reset()

        score = 0

        for step in range(args.max_steps):
            action_dict = agent.act_many(observations)

            observations, all_rewards, done, _ = environment.step(action_dict)

            if args.render == "on":
                environment_renderer.render_env(
                    show=True,
                    show_observations=False,
                    show_predictions=False
                )

            for a in range(args.agent_count):
                score += all_rewards[a]

            observations = next_observations.copy()

            if done["__all__"]:
                break

        tqdm.write(
            f"Trial: [{trial} / {args.num_trials}] - Score: {score}"
        )
