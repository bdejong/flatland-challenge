import torch


def _vector_size(max_depth):
    return sum(12 * 4 ** i for i in range(max_depth + 1))


def vectorize_tree(root, max_depth=2, out=None):
    if out is None:
        out = torch.zeros(_vector_size(max_depth), dtype=torch.float32)

    if root == -float("infinity") or root is None:
        out[:] = -1
        return torch.clamp(out.unsqueeze(dim=0), -1, 100)

    out[0] = root.dist_own_target_encountered
    out[1] = root.dist_other_target_encountered
    out[2] = root.dist_other_agent_encountered
    out[3] = root.dist_potential_conflict
    out[4] = root.dist_unusable_switch
    out[5] = root.dist_to_next_branch
    out[6] = root.dist_min_to_target
    out[7] = root.num_agents_same_direction
    out[8] = root.num_agents_opposite_direction
    out[9] = root.num_agents_malfunctioning
    out[10] = root.speed_min_fractional
    out[11] = root.num_agents_ready_to_depart

    if max_depth > 0:
        child_vector_size = _vector_size(max_depth - 1)

        for i, d in enumerate(root.childs.keys()):
            vectorize_tree(
                root.childs[d],
                max_depth=max_depth - 1,
                out=out[12 + i * child_vector_size: 12 + (i + 1) * child_vector_size]
            )

    return torch.clamp(out.unsqueeze(dim=0), -1, 100)
