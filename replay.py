import numpy as np


class ReplayExperience(object):
    def __init__(self, capacity):
        self._buffer = [None] * capacity
        self._start = 0
        self._amount = 0

    @property
    def capacity(self):
        return len(self._buffer)

    @property
    def count(self):
        return self._amount

    def add(self, item):
        if self._amount < self.capacity:
            self._buffer[self._amount] = item
            self._amount += 1
        else:
            self._buffer[self._start] = item
            self._start = (self._start + 1) % len(self._buffer)

    def sample(self, amount, replacement=False):
        idxs = np.random.choice(self._amount, size=amount, replace=replacement)

        return [self._buffer[idx] for idx in idxs]
