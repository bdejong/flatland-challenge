import copy

import numpy as np
import torch
from abc import ABC, abstractmethod

import replay


class Agent(ABC):
    @abstractmethod
    def __init__(self, state_size, action_size):
        pass

    @abstractmethod
    def act(self, state):
        """
        :param state: input is the observation of the agent.
        :return: returns an action to perform in that state.
        """

        pass

    def act_many(self, states):
        """
        :param state: The observations of the various agents.
        :return: Returns a dictionary of actions for each agent.
        """

        action_dict = dict()

        for a in states.keys():
            action_dict[a] = self.act(states[a])

        return action_dict

    @abstractmethod
    def step(self, memories):
        """
        Step function to update the agents behaviour to more succesfully navigate the environment.

        :param memories:
            Tuple indicating the previous state.
            The action we take in that state.
            The reward we obtained by performing that action.
            The new state we move to by that action.

        :return:
        """

        pass

    def step_many(self, memories):
        states, actions, rewards, next_states, dones = memories

        for a in states.keys():
            self.step((
                states[a],
                actions[a],
                rewards[a],
                next_states[a],
                dones[a]
            ))

    @abstractmethod
    def save(self, filename):
        """
        Store the learned behaviour of the agent at the given location.

        :param filename: Location to store the state of the agent.
        :return:
        """

        pass

    @abstractmethod
    def load(self, filename):
        """
        Load the learned behaviour of the agent at the given location.

        :param filename: Location to load the state of agent from.
        :return:
        """

        pass

    @abstractmethod
    def finish_trial(self):
        """
        Inform the agent that the trial has finished so that it can prepare itself for the next one.

        :return:
        """

        pass

    @abstractmethod
    def eval(self):
        """
        Put the agent in evaluation mode so it won't train itself anymore.

        :return:
        """

        pass


class RandomAgent(object):
    def __init__(self, state_size, action_size):
        self._action_size = action_size

    def act(self, state):
        return np.random.choice(self._action_size, 1)

    def step(self, memories):
        # Since this is a random agent there is nothing to update so this is intentionally left blank.
        pass

    def save(self, filename):
        # Again random agent that doesn't learn anything.
        pass

    def load(self, filename):
        # Again nothing to load due to random behaviour.
        pass

    def finish_trial(self):
        # Same thing nothing necassary.
        pass

    def eval(self):
        # Again nothing to do here.
        pass


class NeuralAgent(Agent):
    def __init__(self, network, batch_size, num_exploration_episodes, base_epsilon, memory_capacity, device, writer):
        self._network = network
        self._target = copy.deepcopy(network)
        self._batch_size = batch_size
        self._base_epsilon = base_epsilon
        self._num_exploration_episodes = num_exploration_episodes

        self._action_size = 5
        self._discount_factor = 0.9

        self._current_trial = 0
        self._step = 0

        self._optimizer = torch.optim.Adam(
            self._network.parameters(),
            lr=1.0e-4
        )
        self._memory = replay.ReplayExperience(memory_capacity)
        self._device = device
        self._writer = writer
        self._evaluate = False

    def _get_epsilon(self):
        epsilon_factor = (self._num_exploration_episodes - min(self._num_exploration_episodes, self._current_trial))
        return (1 - self._base_epsilon) * epsilon_factor / max(1, self._num_exploration_episodes) + self._base_epsilon

    def act(self, observation):
        if np.random.random() < self._get_epsilon() and not self._evaluate:
            return int(np.random.choice(self._action_size, 1))
        else:
            with torch.no_grad():
                return torch.argmax(
                    self._network(
                        observation.to(self._device)
                    ),
                    dim=1
                ).item()

    def act_many(self, observations, mode="single"):
        eps = self._get_epsilon()

        trains = list(observations.keys())

        if np.random.random() < eps:
            return {t: int(np.random.choice(self._action_size, 1)) for t in trains}
        else:
            with torch.no_grad():
                states = torch.cat(
                    [observations[t] for t in trains], dim=0 if mode == "single" else 1
                ).to(self._device)

                q_values = self._network(states)

                if mode == "multi":
                    q_values = q_values.view(-1, self._action_size)

                return {t: torch.argmax(q_values[i]).item() for i, t in enumerate(trains)}

    def _calculate_target(self, reward, done, next_state, discount_factor, mode):
        with torch.no_grad():
            if mode == "multi":
                targets = self._target(next_state).view(next_state.size(0), -1, self._action_size)
            else:
                targets = self._target(next_state)

            return reward + discount_factor * (1 - done.to(dtype=torch.float32)) * torch.max(targets, dim=-1)[0]

    def _calculate_q_val(self, state, action, mode):
        q_values = self._network(state).view(-1, self._action_size)[torch.arange(action.view(-1).size(0)), action.view(-1)]

        if mode == "single":
            return q_values
        else:
            return q_values.view(action.size(0), -1)

    def step(self, transition):
        self._memory.add(transition)

        self._train_network()

    def step_many(self, transitions, mode="single"):
        states, actions, rewards, next_states, dones = transitions

        if mode == "single":
            for a in states.keys():
                self._memory.add((
                    states[a],
                    actions[a],
                    rewards[a],
                    next_states[a],
                    dones[a]
                ))

            self._train_network(mode)
        else:
            keys = list(states.keys())

            state = torch.cat([states[a] for a in keys], dim=1)
            next_state = torch.cat([next_states[a] for a in keys], dim=1)
            reward = [rewards[a] for a in keys]
            action = [actions[a] for a in keys]
            done = [dones[a] for a in keys]

            self._memory.add((
                state,
                action,
                reward,
                next_state,
                done
            ))

            self._train_network(mode)

    def _train_network(self, mode):
        self._step += 1

        if self._memory.count < self._batch_size:
            return

        transitions = self._memory.sample(self._batch_size)
        state, action, reward, next_state, done = zip(*transitions)

        if mode == "single":
            action_tensor = torch.tensor(action, dtype=torch.int64, device=self._device).view(-1)
        else:
            action_tensor = torch.tensor(action, dtype=torch.int64, device=self._device).view(len(action), -1)

        q_val = self._calculate_q_val(
            torch.cat(state, dim=0).to(self._device),
            action_tensor,
            mode=mode
        )

        q_target = self._calculate_target(
            torch.tensor(reward, dtype=torch.float32, device=self._device),
            torch.tensor(done, dtype=torch.bool, device=self._device),
            torch.cat(next_state, dim=0).to(self._device),
            self._discount_factor,
            mode=mode
        )

        loss = torch.nn.functional.smooth_l1_loss(q_val, q_target)

        self._optimizer.zero_grad()
        loss.backward()
        self._optimizer.step()

        with torch.no_grad():
            abs_error = torch.mean(torch.abs(q_val - q_target))

            relative_error = torch.mean(torch.abs(q_val - q_target) / torch.abs(q_val))

        self._writer.add_scalar(
            "agent/network/loss",
            loss.item(),
            self._step
        )
        self._writer.add_scalar(
            "agent/network/absolute-error",
            abs_error.item(),
            self._step
        )
        self._writer.add_scalar(
            "agent/network/relative-error",
            relative_error.item(),
            self._step
        )


    def save(self, filename):
        torch.save(
            {
                "network": self._network.state_dict(),
                "optimizer": self._optimizer.state_dict()
            },
            filename
        )

    def load(self, filename):
        state = torch.load(filename)
        self._network.load_state_dict(
            state["network"]
        )
        self._optimizer.load_state_dict(
            state["optimizer"]
        )

    def finish_trial(self):
        self._current_trial += 1

        self._target = copy.deepcopy(self._network)

    def eval(self):
        self._network.eval()
        self._evaluate = True


class View(torch.nn.Module):
    def __init__(self, *shape):
        super(View, self).__init__()

        self._shape = shape

    def forward(self, x):
        return x.view(*self._shape)


class MlpAgent(NeuralAgent):
    def __init__(self, state_size, action_size, batch_size, num_exploration_episodes, base_epsilon, memory_capacity, device, writer):
        network = torch.nn.Sequential(
            View(-1, state_size),
            torch.nn.Linear(state_size, 256),
            torch.nn.ReLU(),
            torch.nn.Linear(256, 256),
            torch.nn.ReLU(),
            torch.nn.Linear(256, action_size)
        )
        super(MlpAgent, self).__init__(
            network.to(device),
            batch_size,
            num_exploration_episodes,
            base_epsilon,
            memory_capacity,
            device,
            writer
        )


class ConvAgent(NeuralAgent):
    def __init__(self, environment_size, action_size, batch_size, num_exploration_episodes, base_epsilon, memory_capacity, device, writer):
        network = torch.nn.Sequential(
            torch.nn.Conv2d(32, 64, 3, stride=1, padding=1),
            torch.nn.BatchNorm2d(64),
            torch.nn.MaxPool2d(2, 2),
            torch.nn.ReLU(),
            torch.nn.Conv2d(64, 128, 3, stride=1, padding=1),
            torch.nn.BatchNorm2d(128),
            torch.nn.MaxPool2d(2, 2),
            torch.nn.ReLU(),
            torch.nn.Conv2d(128, 256, 5, stride=1),
            torch.nn.ReLU(),
            View(-1, 256),
            torch.nn.Linear(256, action_size)
        )
        super(ConvAgent, self).__init__(
            network.to(device),
            batch_size,
            num_exploration_episodes,
            base_epsilon,
            memory_capacity,
            device,
            writer
        )
