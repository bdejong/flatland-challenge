train.py
--------

Het train script wordt gebruikt om een agent te trainen in de omgeving.

Tijdens het trainen worden allerlei waardes die aangeven hoe het trainen gaat opgeslagen.
Om deze te kunnen bekijken is er tensorboard.

Dat kan je met het volgende command runnen.
```
tensorboard --logdir runs
```

Vervolgens kan je dan daarna http://localhost:6006 om de resultaten te bekijken en te zien hoe goed je model trained.
