import torch

from flatland.core.env_observation_builder import ObservationBuilder
from flatland.envs.agent_utils import RailAgentStatus, EnvAgent
from flatland.envs.observations import TreeObsForRailEnv, LocalObsForRailEnv

import utils

class CoordinateObsForRailEnv(ObservationBuilder):
    """
    Generates observations that consist of an array of 5 values.
    The current position the position of the target location and the direction that the train is facing.
    """
    def __init__(self):
        super().__init__()

    def reset(self):
        pass

    def get(self, handle):
        if handle > len(self.env.agents):
            print("ERROR: obs _get - handle ", handle, " len(agents)", len(self.env.agents))
        agent = self.env.agents[handle]  # TODO: handle being treated as index

        if agent.status == RailAgentStatus.READY_TO_DEPART:
            agent_virtual_position = agent.initial_position
        elif agent.status == RailAgentStatus.ACTIVE:
            agent_virtual_position = agent.position
        elif agent.status == RailAgentStatus.DONE:
            agent_virtual_position = agent.target
        else:
            # Not actually right
            agent_virtual_position = (5, 5)

        return torch.tensor(
            [
                *agent_virtual_position,
                *agent.target,
                agent.direction
            ],
            dtype=torch.float32
        ).unsqueeze(dim=0)


class GlobalObsForRailEnv(ObservationBuilder):
    def __init__(self):
        super(GlobalObsForRailEnv, self).__init__()

    def reset(self):
        rail_map = torch.zeros(16, self.env.width, self.env.height, dtype=torch.float32)
        coordinate_map = torch.stack([
            torch.arange(self.env.width, dtype=torch.float32).view(-1, 1).repeat(1, self.env.height),
            torch.arange(self.env.height, dtype=torch.float32).view(1, -1).repeat(self.env.width, 1)
        ])

        for i in range(rail_map.shape[1]):
            for j in range(rail_map.shape[2]):
                transitions = self.env.rail.get_full_transitions(i, j)
                for k in range(16):
                    rail_map[k, i, j] = int(transitions & (1 << k))

        self._env_map = torch.cat([rail_map, coordinate_map], dim=0)

    def get(self, handle):
        agent = self.env.agents[handle]
        if agent.status == RailAgentStatus.READY_TO_DEPART:
            agent_virtual_position = agent.initial_position
        elif agent.status == RailAgentStatus.ACTIVE:
            agent_virtual_position = agent.position
        elif agent.status == RailAgentStatus.DONE:
            agent_virtual_position = agent.target
        else:
            agent_virtual_position = (-1, -1)

        agent_state = torch.zeros(32, self.env.width, self.env.height, dtype=torch.float32)

        agent_state[0, ...] = agent_virtual_position[0]
        agent_state[1, ...] = agent_virtual_position[1]
        agent_state[2, ...] = agent.target[0]
        agent_state[3, ...] = agent.target[1]
        agent_state[4 + agent.direction] = 1
        
        for a in self.env.agents:
            if a is not agent:
                if a.status == RailAgentStatus.READY_TO_DEPART:
                    pos = a.initial_position
                elif a.status == RailAgentStatus.ACTIVE:
                    pos = a.position
                elif a.status == RailAgentStatus.DONE:
                    pos = a.target
                else:
                    continue

                agent_state[8, pos[0], pos[1]] = a.target[0]
                agent_state[9, pos[0], pos[1]] = a.target[1]
                agent_state[10 + a.direction, pos[0], pos[1]] = 1


        agent_state[14:, ...] = self._env_map

        return agent_state.unsqueeze(dim=0)


class VectorizedTreeObsForRailEnv(TreeObsForRailEnv):
    def __init__(self, max_depth, predictor=None):
        super(VectorizedTreeObsForRailEnv, self).__init__(
            max_depth=max_depth,
            predictor=predictor
        )

    def get(self, handle):
        return utils.vectorize_tree(super().get(handle), self.max_depth)
